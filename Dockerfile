ARG NODE_IMAGE=node:18-alpine

FROM $NODE_IMAGE
WORKDIR /usr/src/app
COPY ../package.json ./package.json
COPY ../yarn.lock ./yarn.lock
RUN yarn install
COPY . .
EXPOSE 5000
ENTRYPOINT [ "node" ]
CMD [ "server.js" ]
