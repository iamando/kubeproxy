# Kubeproxy

## Installation

```bash
# install dps for server on node
yarn install
```

## Scripts

```bash
# build inage on local
docker build -t iamando/kubeproxy-server .

# pull image from server on local
docker pull iamando/kubeproxy-server:latest
```

```bash
# setup minikube on local with docker driver
minikube start --driver=docker
```

```bash
# deploy on minikube
helm install kubeproxy ./deployment --dry-run --debug
```

```bash
# check pod if deployed
kubectl get pods

# check service if deployed
kubectl get svc
```

```bash
# connect to service
minikube service kubeproxy
```

## Support

Kubeproxy is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers.

## License

Kubeproxy is [MIT licensed](LICENSE).
