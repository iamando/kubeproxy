const express = require("express");

const HOST = "0.0.0.0";
const PORT = 5000;

const app = express();

app.get("/", (req, res) => {
    res.send("Node server is running");
});

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});
